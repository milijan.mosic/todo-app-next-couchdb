# CouchDB dockerization example

## Run

#### Add to etc/hosts (sudo nano /etc/hosts):

```
127.0.0.1     couch.mile.com
127.0.0.1     next.mile.com
```

#### Run tests with command:

```bash
docker-compose up --build --remove-orphans
```

#### Look into the database:

#### Open in browser:

```
https://couch.mile.com/_utils/
```

## Explore couchdb API:

```
https://docs.couchdb.org/en/stable/intro/api.html
```
